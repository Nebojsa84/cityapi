﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using CityAPI.Controllers;
using CityAPI.Interfaces;
using CityAPI.Models;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CityAPI.Tests.Controllers
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsProductWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<ICityRepository>();
            mockRepository.Setup(x => x.GetById(9)).Returns(new City { Id = 9 });

            var controller = new CitiesController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetCity(9);
            var contentResult = actionResult as OkNegotiatedContentResult<City>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(9, contentResult.Content.Id);

        }



    }
}
