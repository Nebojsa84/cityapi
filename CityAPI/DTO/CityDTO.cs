﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CityAPI.DTO
{
    public class CityDTO
    {
        public int Population { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }

    }
}