﻿using CityAPI.Interfaces;
using CityAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CityAPI.Controllers
{
    public class CountriesController : ApiController
    {
        public ICountryRepository repository { get; set; }

        public CountriesController( ICountryRepository repository)
        {
            this.repository = repository;
        }


        public IHttpActionResult GetCountries()
        {
            return Ok( repository.GetAll());
        }
        

        public IHttpActionResult GetCountry (int id)
        {
            var country = repository.GetById(id);
            if (country==null)
            {
                return NotFound();
            }

            return Ok(country);
        }

        public IHttpActionResult DeleteCountry(int id)
        {
            var country = repository.GetById(id);

            if(country == null)
            {
                return NotFound();
            }

            repository.Delete(country);
            return StatusCode(HttpStatusCode.NoContent);
        }


        public IHttpActionResult PostCountry(Country country)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            repository.Create(country);
            return CreatedAtRoute("DefaultApi",new { country.Id},country);
        }


        public IHttpActionResult PutCountry(int id, Country country)
        {
            if (id != country.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            repository.Update(country);
            return Ok(country);
        }

    }
}
