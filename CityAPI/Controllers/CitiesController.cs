﻿using CityAPI.DTO;
using CityAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CityAPI.Models;
using AutoMapper;

namespace CityAPI.Controllers
{
    public class CitiesController : ApiController
    {
        public ICityRepository repository { get; set; }

        public CitiesController(ICityRepository repository)
        {
            this.repository = repository;
        }

        public IHttpActionResult GetCities()
        {                  
            return Ok(repository.GetAll().OrderBy(c => c.PostCode));
        }

        public IHttpActionResult GetCities(int from, int to)
        {
           

            var cities = repository.GetAll().Where(c => c.Population >= from && c.Population <= to);

            return Ok(cities);
        }

        public IHttpActionResult GetCity(int id)
        {
            var city = repository.GetById(id);

            if (city==null)
            {
                return NotFound();
            }

            return Ok(city);
        }

        [Route("api/statistics")]
        public IHttpActionResult GetStatistics()
        {
            var cities = Mapper.Map<IEnumerable<City>, IEnumerable<CityDTO>>(repository.GetAll().OrderByDescending(c=>c.Population));
            return Ok(cities);
        }

        public IHttpActionResult PostCities(City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            repository.Create(city);
            return CreatedAtRoute("DefaultApi", new { city.Id }, city);
        }

        public IHttpActionResult DeleteCity(int id)
        {
            var city = repository.GetById(id);
            if (city==null)
            {
                return NotFound();
            }

            repository.Delete(city);
            return StatusCode(HttpStatusCode.NoContent);
        }

        public IHttpActionResult PutCity(int id, City city)
        {
            if(id != city.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            repository.Update(city);
            return Ok(city);
        }

    }
}
