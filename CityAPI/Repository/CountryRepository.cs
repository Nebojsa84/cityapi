﻿using CityAPI.Interfaces;
using CityAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace CityAPI.Repository
{
    public class CountryRepository : ICountryRepository, IDisposable
    {
        CityDbContext db = new CityDbContext();

        public void Create(Country country)
        {
            db.Countries.Add(country);
            db.SaveChanges();
        }

        public void Delete(Country country)
        {
            db.Countries.Remove(country);
            db.SaveChanges();
        }


        public IEnumerable<Country> GetAll()
        {
            return db.Countries;
        }

        public Country GetById(int id)
        {
            return db.Countries.FirstOrDefault(c => c.Id == id);
        }

        public void Update(Country country)
        {
            db.Entry(country).State =EntityState.Modified;

            try
            {
                db.SaveChanges();

            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}