﻿using CityAPI.Interfaces;
using CityAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace CityAPI.Repository
{
    public class CityRepository : ICityRepository, IDisposable
    {
        CityDbContext db = new CityDbContext();

        public void Create(City city)
        {
            db.Cities.Add(city);
            db.SaveChanges();
        }

        public void Delete(City city)
        {
            db.Cities.Remove(city);
            db.SaveChanges();
        }


        public IEnumerable<City> GetAll()
        {
            return db.Cities.Include(c=>c.Country);
        }

        public City GetById(int id)
        {
            return db.Cities.Include(c=>c.Country).FirstOrDefault(c => c.Id == id);
        }

        public void Update(City city)
        {
            db.Entry(city).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}