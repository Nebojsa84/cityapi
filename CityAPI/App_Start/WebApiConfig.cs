﻿
using AutoMapper;
using CityAPI.DTO;
using CityAPI.Interfaces;
using CityAPI.Models;
using CityAPI.Repository;
using CityAPI.Resolver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Unity;
using Unity.Lifetime;

namespace CityAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var container = new UnityContainer();
            container.RegisterType<ICityRepository, CityRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ICountryRepository, CountryRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);


            Mapper.Initialize(cfg => {
                cfg.CreateMap<City, CityDTO>();
               
            });


        }
    }
}
