namespace CityAPI.Migrations
{
    using CityAPI.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CityAPI.Models.CityDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CityAPI.Models.CityDbContext context)
        {
            context.Countries.AddOrUpdate(x => x.Id,
                 new Country() { Id = 1, Name = "Serbia", Code = "SRB" },
                 new Country() { Id = 2, Name = "Germany", Code = "DEU" },
                 new Country() { Id = 3, Name = "Italy", Code = "ITA" },
                 new Country() { Id = 4, Name = "Sweden", Code = "SWE" }
                 );

            context.Cities.AddOrUpdate(x => x.Id,
                new City()
                {
                    Id = 1,
                    Name = "Belgrade",
                    PostCode = 11000,
                    Population = 1300000,
                    CountryId=1
                },              
                new City()
                {
                    Id = 3,
                    Name = "Rome",
                    PostCode = 58091,
                    Population = 2872800,
                    CountryId = 3

                },
               
                 new City()
                 {
                     Id = 5,
                     Name = "Berlin",
                     PostCode = 10117,
                     Population = 3711930,
                     CountryId = 2

                 },

                 new City()
                 {
                     Id = 7,
                     Name = "Stockholm",
                     PostCode = 18420,
                     Population = 952058,
                     CountryId = 4

                 }

                
                );
        }
    }
}
