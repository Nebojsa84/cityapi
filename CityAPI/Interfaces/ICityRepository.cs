﻿using CityAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CityAPI.Interfaces
{
    public interface ICityRepository
    {
        IEnumerable<City> GetAll();
        City GetById(int id);
        void Create(City city);
        void Update(City city);
        void Delete(City city);
    }
}