﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CityAPI.Models
{
    public class City
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Range(10000,99999)]
        public int PostCode { get; set; }
        [Range(1,int.MaxValue)]
        public int Population { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }

    }
}